﻿using OpenQA.Selenium;
using static PageObjectModel.Utilities.Selenium.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.Pages
{
    public class BasePage : Page
    {
        public IWebDriver Driver { get; internal set; }

        public void NavigateMainEnterPoint()
        {
            const string url = "https://www.wikipedia.org/";
            Browser().Navigate().GoToUrl(url);
            Browser().Manage().Window.Maximize();
        }
    }

    
}
