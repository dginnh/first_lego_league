﻿using PageObjectModel.Pages;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class BaseScenarioSteps : BaseSteps
    {
        [Given(@"I navigate to the homepage")]
        public void GivenINavigateToTheHomepage() => InstanceOf<BasePage>().NavigateMainEnterPoint();


    }
}
