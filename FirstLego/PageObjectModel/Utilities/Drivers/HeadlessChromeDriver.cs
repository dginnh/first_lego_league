﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace PageObjectModel.Utilities.Drivers
{
    internal static class HeadlessChromeDriver
    {

        internal static IWebDriver LoadChromeDriver()
        {
            // Hide command prompt window when starting up the chrome web driver
            var driverService = ChromeDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;

            // Disable chrome extensions and run headless when starting up the chrome web driver
            var options = new ChromeOptions();
            options.AddArgument("--disable-extensions");
            options.AddArguments("headless");

            // Start and return the chrome driver instance
            var driver = new ChromeDriver(driverService, options);
            return driver;
        }
    }
}
