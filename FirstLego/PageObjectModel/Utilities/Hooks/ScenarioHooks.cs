﻿using PageObjectModel.Utilities.Selenium;
using System.Linq;
using TechTalk.SpecFlow;

namespace PageObjectModel.Utilities.Hooks
{
    [Binding]
    internal static class ScenarioHooks
    {
        [BeforeScenario]
        internal static void StartWebDriver()
        {
            if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("HeadlessChrome"))
            {
                DriverController.Instance.StartHeadlessChrome();
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("Chrome"))
            {
                DriverController.Instance.StartChrome();
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("Debug"))
            {
                DriverController.Instance.StartChrome();
            }
            else
            {
                DriverController.Instance.StartChrome();
            }
        }

        [AfterScenario]
        internal static void StopWebDriver()
        {
            if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("Debug"))
            {
                DriverController.Instance.StopWebDriver();
            }
        }
    }
}
