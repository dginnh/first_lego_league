﻿using PageObjectModel.Utilities.Selenium;
using System.Linq;
using TechTalk.SpecFlow;

namespace PageObjectModel.Utilities.Hooks
{
    [Binding]
    internal static class TestRunHooks
    {
        [AfterTestRun]
        internal static void AfterTestRun()
        {
            if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("Debug"))
            {
                DriverController.Instance.StopWebDriver();
            }
        }
    }
}
