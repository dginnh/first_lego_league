﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PageObjectModel.Utilities.Selenium
{
    [Binding]
    internal class Driver
    {
        internal static IWebDriver Browser() => DriverController.Instance.WebDriver;
    }
}
