﻿using OpenQA.Selenium;
using PageObjectModel.Utilities.Drivers;
using System;
using System.Diagnostics;


namespace PageObjectModel.Utilities.Selenium
{
    internal class DriverController
    {
        internal static DriverController Instance = new DriverController();

        internal IWebDriver WebDriver { get; set; }

        internal void StartChrome()
        {
            if (WebDriver != null) return;
            WebDriver = ChromeWebDriver.LoadChromeDriver();
        }

        internal void StartHeadlessChrome()
        {
            if (WebDriver != null) return;
            WebDriver = HeadlessChromeDriver.LoadChromeDriver();
        }

        internal void StopWebDriver()
        {
            if (WebDriver == null) return;

            try
            {
                WebDriver.Quit();
                WebDriver.Dispose();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e, ":: WebDriver stop error");
                throw;
            }
        }
    }
}
